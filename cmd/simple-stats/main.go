package main

import "gitlab.oit.duke.edu/drews/simple-stats/cmd/simple-stats/cmd"

func main() {
	cmd.Execute()
}
