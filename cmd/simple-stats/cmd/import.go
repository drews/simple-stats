package cmd

import (
	"os"

	"github.com/spf13/cobra"
	stats "gitlab.oit.duke.edu/drews/simple-stats"
)

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import",
	Short: "Import some metrics",
	Run: func(cmd *cobra.Command, args []string) {
		s := stats.New(
			stats.WithInit(),
			stats.WithDSN(os.Getenv("STATS_DB")),
		)
		labels := stats.Labels{"foo": "bazinga"}
		m := stats.Metric{
			Name:  "another-foo",
			Time:  44444,
			Value: 45,
		}
		err := m.Labels.Set(labels)
		checkErr(err, "error setting labels")
		err = s.Insert(m)
		checkErr(err, "Error Inserting metric")
	},
}

func init() {
	rootCmd.AddCommand(importCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// importCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// importCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
