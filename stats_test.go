package stats

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	// Make sure they at least set the DSN
	require.Panics(t, func() { New() })
	require.NotPanics(t, func() { New(WithDSN("foo")) })
}
