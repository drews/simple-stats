/*
Package stats represents all the statitic...stuff
*/
package stats

import (
	"errors"

	"github.com/jackc/pgtype"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

// Labels is a map of strings containing strings
type Labels map[string]string

// Metric is a single metric entry
type Metric struct {
	gorm.Model
	ID     uint         `gorm:"primaryKey"`
	Time   int64        `json:"time" gorm:"index:uni,unique"`
	Name   string       `json:"name" gorm:"index:uni,unique"`
	Labels pgtype.JSONB `gorm:"type:jsonb;default:'{}';not null;index:uni,unique"`
	Value  float64      `json:"value"`
}

// Metrics is multiple Metric objects
type Metrics []Metric

// Stat interacts with all the statistics IO
type Stat struct {
	dsn   string
	table string
	db    *gorm.DB
	init  bool
}

// WithDSN sets the dsn in a Stat object
func WithDSN(d string) func(*Stat) {
	return func(s *Stat) {
		s.dsn = d
	}
}

// WithInit initializes the DB on new
func WithInit() func(*Stat) {
	return func(s *Stat) {
		s.init = true
	}
}

// WithTable sets a non-default table for the metrics
func WithTable(t string) func(s *Stat) {
	return func(s *Stat) {
		s.table = t
	}
}

// New uses functional options to create a new Stat object
func New(options ...func(*Stat)) *Stat {
	s := &Stat{
		table: "metrics",
	}
	for _, o := range options {
		o(s)
	}
	if s.dsn == "" {
		panic("must set DSN")
	}
	if s.init {
		var err error
		s.db, err = s.initDB()
		panicIfErr(err)
	}
	return s
}

// Insert inserts a new metric
func (s Stat) Insert(ms ...Metric) error {
	// return s.db.Insert(&m)
	// Does this already exist?
	if s.db == nil {
		return errors.New("stat object has no db set")
	}
	for _, m := range ms {
		m := m
		tx := s.db.Clauses(clause.OnConflict{
			Columns: []clause.Column{
				{Name: "name"},
				{Name: "time"},
				{Name: "labels"},
			},
			DoUpdates: clause.Assignments(map[string]interface{}{
				"value": m.Value,
			}),
		}).Create(&m)
		_ = tx
	}

	return nil
}

// initDB initializes the database, if it has not yet been initialized
func (s Stat) initDB() (*gorm.DB, error) {
	db, err := gorm.Open(postgres.Open(s.dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	err = db.AutoMigrate(&Metric{})
	if err != nil {
		return nil, err
	}

	return db, nil
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
